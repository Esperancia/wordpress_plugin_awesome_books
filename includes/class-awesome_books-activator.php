<?php

/**
 * Fired during plugin activation
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Awesome_books
 * @subpackage Awesome_books/includes
 * @author     Pouch <champouxah@yahoo.fr>
 */
class Awesome_books_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

			// global $wpdb;
			// global $plugin_table_name;
			//
			// $plugin_table_name = $wpdb->prefix . 'books';
			//
			// $charset_collate = $wpdb->get_charset_collate();
			//
			// if($wpdb->get_var("show tables like '$plugin_table_name'") != $plugin_table_name)
			// {
			// 	$sql = "CREATE TABLE " . $plugin_table_name . " (
			// 	`id` mediumint(9) NOT NULL AUTO_INCREMENT,
			// 	`Nom` varchar(55) NOT NULL,
			// 	`Auteur` varchar(55) NOT NULL,
			// 	`Nbre_pages` int NOT NULL,
			// 	`Categorie` tinytext NOT NULL,
			// 	`Resume` text NOT NULL,
			// 	`Couverture` varchar(55) NOT NULL,
			// 	`Validation` boolean NOT NULL,
			// 	UNIQUE KEY id (id)
			// 	);";
			//
			// 	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			// 	dbDelta($sql);
			// }

			/* Custom post type */

	}

}
