<?php

/**
 * Fired during plugin deactivation
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Awesome_books
 * @subpackage Awesome_books/includes
 * @author     Pouch <champouxah@yahoo.fr>
 */
class Awesome_books_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
