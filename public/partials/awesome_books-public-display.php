<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="form">
  <h2> Ajoutez vos livres </h2>
    <div id="messages"></div>
    <form id="book-form">
    <div>
    <label for="Nom">Nom <strong>*</strong></label>
    <input type="text" name="nom" id="nom" value="" required>
    </div>

    <div>
    <label for="Auteur">Auteur <strong>*</strong></label>
    <input type="text" name="auteur" id="auteur" value="" required>
    </div>

    <div>
    <label for="Nbre_pages">Nbre_pages <strong>*</strong></label>
    <input type="number" name="nbre_pages" id="nbre_pages" value="" required>
    </div>

    <div>
    <label for="Categorie">Catégorie <strong>*</strong></label>
    <input type="text" name="categorie" id="categorie" value="" required>
    </div>

    <div>
    <label for="Resume">Resume <strong>*</strong></label>
    <textarea name="resume" id="resume" required></textarea>
    </div>

    <div>
    <label for="Couverture">Couverture <strong>*</strong></label>
    <input type="file" name="couverture" id="couverture" value="" required>
    </div>

    <br>
    <button type="button">Enregistrer</button>
    </form>
</div>
