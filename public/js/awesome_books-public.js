(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	 jQuery(document).ready(function(){
		 $('#book-form button').click(function(e){
	    var nom = $("#nom").val();
	    var auteur = $("#auteur").val();
	    var nbre_pages = $("#nbre_pages").val();
	    var categorie = $("#categorie").val();
	    var resume = $("#resume").val();
	    $.ajax({
	         data: {action: 'save_book', nom:nom, auteur:auteur, nbre_pages:nbre_pages, categorie:categorie, resume:resume},
	         type: 'POST',
	         url: MyAjax.ajaxurl,
	         success: function(data) {
	          // alert(data);
						// console.log("tic tac");
						$("#book-form")[0].reset();
						$("#messages").html("Votre livre a été bien enregistré!")
	        },
					error: function(jqXHR, textStatus, errorThrown) {
	          console.log(jqXHR, textStatus, errorThrown);
	        }
	    });
    });

	});

})( jQuery );
