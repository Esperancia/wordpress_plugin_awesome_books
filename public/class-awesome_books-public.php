<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/public
 * @author     Pouch <champouxah@yahoo.fr>
 */
class Awesome_books_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awesome_books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awesome_books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/awesome_books-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awesome_books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awesome_books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/awesome_books-public.js', array( 'jquery' ), $this->version, false );
		// including ajax script in the plugin Myajax.ajaxurl
		wp_localize_script( $this->plugin_name, 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));

	}

	public function awesome_books_form() {
		if(!is_user_logged_in()) {
			echo '<script> window.location = "'.wp_login_url( get_permalink() ).'"; </script>';
		}
		else {
			return $this->awesome_books_form_fields();
		}
	}

	public function awesome_books_form_fields() {
		include_once 'partials/awesome_books-public-display.php';
	}

	public function save_book() {

		extract($_POST);

		$post = array(
			'title' => $nom,
			'auteur' => $auteur,
			'nbre_pages' => $nbre_pages,
			'categorie' => $categorie,
			'content' => $resume,
			'_wp_attached_file' => $couverture,
			'post_status' => 'pending',            // Choose: publish, preview, future, etc.
			'post_type' => 'livre'
		);

		wp_insert_post($post);
		die();
		return true;

	}

}
