<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Awesome_books
 * @subpackage Awesome_books/admin
 * @author     Pouch <champouxah@yahoo.fr>
 */

class Awesome_books_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */


  /**
 	 * The options name to be used in this plugin
 	 *
 	 * @since  	1.0.0
 	 * @access 	private
 	 * @var  	string 		$option_name 	Option name of this plugin
 	 */
  private $option_name = 'awesome_books';

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awesome_books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awesome_books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/awesome_books-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Awesome_books_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Awesome_books_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/awesome_books-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * custom_post_book definition
	 *
	 * @since  1.0.0
	 */
	public function custom_post_book() {
		register_post_type( 'livre',
	    array(
	      'labels' => array(
	        'name' => __( 'Livres' ),
	        'singular_name' => __( 'Livre' ),
					'menu_name' => __( 'Livres' ),
					'all_items' => __( 'Tous les livres' ),
					'view_item' => __( 'Voir le livre' ),
					'add_new_item' => __( 'Ajouter nouveau livre' ),
					'edit_item' => __( 'Modifier le livre' ),
					'update_item' => __( 'Mettre à jour le livre' )
        ),
				// Features this CPT supports in Post Editor
				'supports' => array( 'title', 'editor', 'thumbnail' ),
	      'public' => true,
				'rewrite' => array('slug' => 'livres')
	    )
  	);
	}

	/**
	 * fields for custom_post_book definition
	 *
	 * @since  1.0.0
	 */
	public function custom_post_book_fields() {
		add_meta_box('categorie', 'Catégorie du livre', array($this, 'getCategorie'), 'livre', 'side', 'high', null);
		add_meta_box('auteur', 'Auteur', array($this, 'getAuteur'), 'livre', 'side', 'high', null);
  	add_meta_box('nbre_pages', 'Nombre de pages', array($this, 'getNbre_pages'), 'livre', 'side', 'high', null);
	}

	/* fonctions callback for book custom fields */
	public function getCategorie(){
	  global $post;
		//var_dump($post);
	  $custom = get_post_custom($post->ID);
		$categorie = '';
		if (isset ( $custom['categorie'][0] )) {
	  	$categorie = $custom['categorie'][0];
		}
	  ?>
	  <input name='categorie' value='<?php echo $categorie; ?>' />
	  <?php
	}

	public function getAuteur(){
	  global $post;
	  $custom = get_post_custom($post->ID);
		$auteur = '';
		if (isset ( $custom['auteur'][0] )) {
	  	$auteur = $custom['auteur'][0];
		}
	  ?>
	  <input name='auteur' value='<?php echo $auteur; ?>' />
	  <?php
	}

	public function getNbre_pages(){
	  global $post;
	  $custom = get_post_custom($post->ID);
		$nbre_pages = 0;
		if (isset ( $custom['nbre_pages'][0] )) {
	  	$nbre_pages = $custom['nbre_pages'][0];
		}
	  ?>
	  <input name='nbre_pages' type='number' value='<?php echo $nbre_pages; ?>' />
	  <?php
	}


	/**
	 * save custom_post_book
	 *
	 * @since  1.0.0
	 */
	public function save_book($post_id){

		var_dump($post_id);

	  update_post_meta($post_id, 'categorie', isset($_POST['categorie']) ? $_POST['categorie'] : '');
	  update_post_meta($post_id, 'auteur', isset($_POST['auteur']) ? $_POST['categorie'] : '');
	  update_post_meta($post_id, 'nbre_pages', isset($_POST['nbre_pages']) ? $_POST['categorie'] : '');
	}


}
