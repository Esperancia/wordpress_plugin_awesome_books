<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              example.com
 * @since             1.0.0
 * @package           Awesome_books
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome_books
 * Plugin URI:        http://wordpress.org/plugins/awesome_books
 * Description:       This plugin allows user to add books and displays books to admin. Shortcode used: [book_form].
 * Version:           1.0.0
 * Author:            Pouch
 * Author URI:        example.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       awesome_books
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-awesome_books-activator.php
 */
 /*
 	creer la table des libres.
 */
function activate_awesome_books() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-awesome_books-activator.php';
	Awesome_books_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-awesome_books-deactivator.php
 */
function deactivate_awesome_books() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-awesome_books-deactivator.php';
	Awesome_books_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_awesome_books' );
register_deactivation_hook( __FILE__, 'deactivate_awesome_books' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-awesome_books.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_awesome_books() {

	$plugin = new Awesome_books();
	$plugin->run();

}
run_awesome_books();
